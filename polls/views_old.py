from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from reportlab.pdfbase import pdfmetrics  # Чтобы русский шрифт не заменялся черными квадратиками,
from reportlab.pdfbase.ttfonts import TTFont  # надо добавить эти строчки
from reportlab.pdfgen import canvas
from django.shortcuts import get_object_or_404, render

from .models import Choice, Question


def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=
(question.id, )))

def pdf(request):
    # Create the HttpResponse object with the appropriate PDF headers.
    pdfmetrics.registerFont(TTFont('Verdana', '/home/tin/verdana.ttf'))  # и вот эту
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response)
    p.setFont("Verdana", 16)  # и, наконец, эту.

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 400, "Hello world (или мир, кому как нравится).")

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()
    return response
